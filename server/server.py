# coding=utf-8
import argparse
import json
import sys
from threading import Lock, Thread
import time
import traceback
import bottle
from bottle import Bottle, request, template, run, static_file
import requests
import queue
from functools import cmp_to_key
import numpy as np
from random import choice
from string import ascii_uppercase

# ------------------------------------------------------------------------------------------------------

class Blackboard():


    def __init__(self):
        self.lastId = 0
        self.content = dict()    
        self.lock = Lock() # use lock when you modify the content


    def get_content(self):
        with self.lock:
            cnt = self.content
            
        return cnt


    def set_content(self, new_content):
        with self.lock:
            self.content[self.lastId] = new_content
            self.lastId = self.lastId + 1
        return

    def set_content(self, key, new_content):
        with self.lock:
            self.content[key] = new_content
        return

    def edit_content(self, key, new_content):
        with self.lock:
            self.content[key] = new_content
        return

    def delete_content(self, key):
        with self.lock:
            del self.content[key]
        return

# ------------------------------------------------------------------------------------------------------

class VectorClock():
    def __init__(self, clock, currentClock, senderId, value):
        self.clock = clock
        self.lastUpdatedClock = [0] * 8
        self.senderId = senderId  
        self.value = value
        self.sum = sum(clock)
        self.currentClock = currentClock
        self.clockStr = ''.join(str(e) for e in self.clock)


    def __str__(self):
        return "C{} V{} C{}".format(self.clock, self.value, self.clockStr) 
        
    def __repr__(self):
        return self.__str__()        

class Server(Bottle):

    def __init__(self, ID, IP, servers_list):
        super(Server, self).__init__()
        self.blackboard = Blackboard()
        self.id = int(ID)
        self.ip = str(IP)
        self.servers_list = servers_list
        self.NUMBER_OF_NODES = 8
        self.lock = Lock()
        self.vectorClock = []
        # self.historyClock = []
        self.data = []
        self.pending = dict()
        self.pendingData = []

        # list all REST URIs
        # if you add new URIs to the server, you need to add them here
        self.route('/', callback=self.index)
        self.get('/board', callback=self.get_board)
        self.post('/board', callback=self.post_index)
        self.post('/board/<element_id:int>/', callback=self.editDeleteBoard)
        self.post('/propogate/data/', callback=self.dataHandler)
        self.post('/propogate/update/', callback=self.updateHandler)
        self.post('/pending/', callback=self.pendingDataHandler)
        # we give access to the templates elements
        self.get('/templates/<filename:path>', callback=self.get_template)


    def do_parallel_task(self, method, args=None):
        # create a thread running a new task
        # Usage example: self.do_parallel_task(self.contact_another_server, args=("10.1.0.2", "/index", "POST", params_dict))
        # this would start a thread sending a post request to server 10.1.0.2 with URI /index and with params params_dict
        thread = Thread(target=method,
                        args=args)
        thread.daemon = True
        thread.start()


    def do_parallel_task_after_delay(self, delay, method, args=None):
        # create a thread, and run a task after a specified delay
        # Usage example: self.do_parallel_task_after_delay(10, self.start_election, args=(,))
        # this would start a thread starting an election after 10 seconds
        print("inside do_parallel_task_after_delay")
        thread = Thread(target=self._wrapper_delay_and_execute,
                        args=(delay, method, args))
        thread.daemon = True
        thread.start()


    def _wrapper_delay_and_execute(self, delay, method, args):
        time.sleep(delay) # in sec
        method(*args)


    def contact_another_server(self, srv_ip, URI, req='POST', params_dict=None):
        # Try to contact another serverthrough a POST or GET
        # usage: server.contact_another_server("10.1.1.1", "/index", "POST", params_dict)
        success = False
        try:
            if 'POST' in req:
                res = requests.post('http://{}{}'.format(srv_ip, URI),
                                    data=params_dict)
            if 'PUT' in req:
                res = requests.put('http://{}{}'.format(srv_ip, URI),
                                    data=params_dict)
            elif 'GET' in req:
                res = requests.get('http://{}{}'.format(srv_ip, URI))
            elif 'DELETE' in req:
                res = requests.delete('http://{}{}'.format(srv_ip, URI))
            # result can be accessed res.json()
            if res.status_code == 200:
                success = True
        except Exception as e:
            print("[ERROR] "+str(e))
        return success


    def getAccessLock(self):
        with self.lock:
            return self.accessLock

    def setAccessLock(self, lock):
        with self.lock:
            self.accessLock = lock

    def propagate_to_all_servers(self, URI, req='POST', params_dict=None):
        for srv_ip in self.servers_list:
            if srv_ip != self.ip: # don't propagate to yourself
                print("request to " + srv_ip + URI)
                success = self.contact_another_server(srv_ip, URI, req, params_dict)

                if not success:
                    data = {
                        "clock": params_dict["clock"],
                        "type": params_dict["type"],
                        "ip": srv_ip,
                        "id": params_dict["id"]
                    }

                    if params_dict["type"] == "INSERT":
                        data["entry"] =  params_dict["entry"]
                    else:
                        data["dataClock"] =  params_dict["dataClock"]
                    
                    random = ''.join(choice(ascii_uppercase) for i in range(8))
                    self.pending[random] = data

                    print("[WARNING ]Could not contact server {}".format(srv_ip))


    # route to ('/')
    def index(self):
        # we must transform the blackboard as a dict for compatiobility reasons
        board = dict()
        board = self.blackboard.get_content()
        
        return template('server/templates/index.tpl',
                        board_title='Server {} ({})'.format(self.id,
                                                            self.ip),
                        board_dict=board.items(),
                        members_name_string='INPUT YOUR NAME HERE')

    # get on ('/board')
    def get_board(self):
        # we must transform the blackboard as a dict for compatibility reasons
        board = dict()
        # board = self.blackboard.get_content()   
        customList = []
        
        for index in range(len(self.data)):
            #board[index] = self.data[index].value 
            customList.append( (index, self.data[index].value, self.data[index].currentClock) )	     
	     
        return template('server/templates/blackboard.tpl',
                        board_title='Server {} ({}) Server Clock ({})'.format(self.id,
                                                            self.ip, self.vectorClock),
                        board_dict=customList)


    # post on ('/')
    def post_index(self):
        try:
            
            # we read the POST form, and check for an element called 'entry'
            new_entry = request.forms.get('entry')
            self.incrementClock()
            
            vectorCopy = self.vectorClock[:]
            self.data.insert(len(self.data), VectorClock(vectorCopy, vectorCopy, self.id, new_entry))    
            jsonStr = json.dumps(self.vectorClock)                
            self.do_parallel_task(self.sendDataAndClock, args=(new_entry, jsonStr, ))

        except Exception as e:
            print("[ERROR] "+str(e))

    def sendDataAndClock(self, entry, clock):
        data = {'entry': entry, 'clock': clock, 'id': self.id, 'type': "INSERT"}
        self.propagate_to_all_servers("/propogate/data/", "POST", data)


    # Edit or Delete from the board
    def editDeleteBoard(self, element_id):
        try:
             
            action = request.forms.get('delete')
            new_entry = request.forms.get('entry')
            self.incrementClock()
            print("here0", element_id)
            # index = next((i for i, item in enumerate(self.data) if str(element_id) == item.clockStr))
            index = element_id
            data = {
                "dataClock": json.dumps(self.data[index].clock),
                "clock": json.dumps(self.vectorClock),
                "id": self.id
            }

            if action == '0':
                data['entry'] = new_entry
                data['type'] = 'UPDATE'
                self.data[index].value = new_entry    
                vectorCopy = self.vectorClock[:]           
                self.data[index].lastUpdatedClock = vectorCopy

            elif action == '1':
                print("here2")
                data['type'] = 'DELETE'
                del self.data[index]
                print("here3")
                
            self.do_parallel_task(self.updateElement, args=(data, ))
        
        except Exception as e:
            print("[ERROR] "+str(e))
        
    def updateElement(self, data):
        self.propagate_to_all_servers("/propogate/update/", "POST", data)
        

    def get_template(self, filename):
        return static_file(filename, root='./server/templates/')    

    # Custom comparator for sorting  
    def compare(self, clock1, clock2):
        c1 = clock1.clock
        c2 = clock2.clock
        idx = range(len(c1))
        # or 
        if self.lessThen(c1, c2):
            return 1
        elif self.isConcurrent(c1, c2) and (clock1.sum < clock2.sum):
            return 1
        elif self.isConcurrent(c1, c2) and clock1.sum == clock2.sum and clock1.senderId < clock2.senderId:
            return 1
        else: 
            return -1    

    # Check if clock is less then the other
    def lessThen(self, c1, c2):
        idx = range(len(c1))       
        return all([c1[i] <= c2[i] for i in idx]) and any([c1[i] < c2[i] for i in idx])

    # Check if clocks are concurrent
    def isConcurrent(self, myclock, otherclock):
        greater = False
        less = False
        for index in range(len(myclock)):
            if myclock[index] > otherclock[index]:
                greater = True
            elif myclock[index] <= otherclock[index]:
                less = True
                
        if greater and less:
            return True
        else:
            return False


    def tryPending(self):
        while True:
            print("Try Pending ", len(self.pending))
            if self.pending:
                for key in list(self.pending.keys()):
                    obj = self.pending[key]
                    print("sending pending data", obj)
                    success = self.contact_another_server(obj["ip"], "/pending/", "POST", obj)
                    if success:
                        print("SUccess", self.pending[key])
                        del self.pending[key]   
            time.sleep(20) 

           

    #init vector
    def initVector(self):
        print("Vector Init")
        self.vectorClock = [0] * 8
        self.do_parallel_task(self.tryPending, args=())

    #increment clock
    def incrementClock(self):
        index = self.id - 1
        self.vectorClock[index] = self.vectorClock[index] + 1

    # modify and delete handler
    def updateHandler(self):
        print("Inside update Handler")
        dataClock = json.loads(request.forms.get('dataClock'))
        otherClock = json.loads(request.forms.get('clock'))
        type = request.forms.get('type')
        entry = request.forms.get('entry')
        self.mergeClock(otherClock)
        index = next((i for i, item in enumerate(self.data) if np.array_equal(item.clock, dataClock, -1)))

        if index != -1:        
            result = self.data[index]

            if self.lessThen(result.lastUpdatedClock, otherClock):
                if type == 'UPDATE':                
                    result.lastUpdatedClock = otherClock
                    result.value = entry

                elif type == 'DELETE':
                    del self.data[index]
        else :
            print("fail")

    def dataHandler(self):
        print("inside data handler")
        entry = request.forms.get('entry')
        clock = request.forms.get('clock')
        senderId = request.forms.get('id')
        otherClock = json.loads(clock)
        self.mergeClock(otherClock)
        copyVectorClock = self.vectorClock[:]
        self.data.insert(len(self.data), VectorClock(otherClock, copyVectorClock, int(senderId), entry))
        # sort data
        self.data = sorted(self.data, key=cmp_to_key(self.compare), reverse=True)
            
        
    # Pending Data Handler 
    def pendingDataHandler(self):
        print("Inside Pending Data Handler")
        type = request.forms.get('type')
        clock = json.loads(request.forms.get('clock'))
        id = request.forms.get('id')
        ip = request.forms.get('ip')
        entry = request.forms.get('entry')
        dataClock = request.forms.get('dataClock')
        self.mergeClock(clock)
        if type == "INSERT":
            self.data.insert(len(self.data), VectorClock(clock, self.vectorClock, id, entry)) 
            self.data = sorted(self.data, key=cmp_to_key(self.compare), reverse=True)


    def mergeClock(self, otherClock):
        
        index = self.id - 1
        self.vectorClock[index] = self.vectorClock[index] + 1  

        for index in range(len(otherClock)):
            if otherClock[index] >= self.vectorClock[index]:
                 self.vectorClock[index] = otherClock[index]


# ------------------------------------------------------------------------------------------------------
def main():
    PORT = 80
    parser = argparse.ArgumentParser(description='Your own implementation of the distributed blackboard')
    parser.add_argument('--id',
                        nargs='?',
                        dest='id',
                        default=1,
                        type=int,
                        help='This server ID')
    parser.add_argument('--servers',
                        nargs='?',
                        dest='srv_list',
                        default="10.1.0.1,10.1.0.2",
                        help='List of all servers present in the network')
    args = parser.parse_args()
    server_id = args.id
    server_ip = "10.1.0.{}".format(server_id)
    servers_list = args.srv_list.split(",")

    try:
        server = Server(server_id, server_ip, servers_list) 

        server.initVector()

        bottle.run(server, host=server_ip, port=PORT)
    except Exception as e:
        print("[ERROR] "+str(e))


# ------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
